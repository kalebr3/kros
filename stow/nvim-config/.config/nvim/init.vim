set nocompatible
syntax enable

"""""""""""""""""""""""""""""""""""""""""""""""""
" Import Plugins Using Vim-Plug
"""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin()

    Plug 'jeffkreeftmeijer/vim-dim'
    Plug 'itchyny/lightline.vim'
    Plug 'scrooloose/nerdtree'
    Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
    Plug 'ryanoasis/vim-devicons'
    Plug 'tpope/vim-surround'
    Plug 'ap/vim-css-color'
    Plug 'neovim/nvim-lspconfig'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""
" General
"""""""""""""""""""""""""""""""""""""""""""""""""
set encoding=utf-8
set title
set path+=**
set wildignore+=*/node_modules/*
set incsearch
set hidden
set number
set noshowmode
set showtabline=2
set wildmenu
set nobackup
set noswapfile
set clipboard=unnamedplus
set shell=zsh
set ignorecase
set cursorline

"""""""""""""""""""""""""""""""""""""""""""""""""
" Text, Tab, Space
"""""""""""""""""""""""""""""""""""""""""""""""""
set autoindent
set smartindent
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""
" Colors and Theming
"""""""""""""""""""""""""""""""""""""""""""""""""
colorscheme dim
let g:lightline = { 'colorscheme': 'wombat' }

"""""""""""""""""""""""""""""""""""""""""""""""""
" Mouse Scrolling
"""""""""""""""""""""""""""""""""""""""""""""""""
set mouse=nicr
set mouse=a

"""""""""""""""""""""""""""""""""""""""""""""""""
" Key Bindings
"""""""""""""""""""""""""""""""""""""""""""""""""
map <C-n> :NERDTreeToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree
"""""""""""""""""""""""""""""""""""""""""""""""""
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI=1
let g:NERDTreeWinSize=38

