module Colors.Nord where

import XMonad

colorScheme = "nord"

colorBack = "#2E3440"
colorFore = "#D8DEE9"

color01 = "#343D46"
color02 = "#EC5F67"
color03 = "#99C794"
color04 = "#FAC863"
color05 = "#6699CC"
color06 = "#C594C5"
color07 = "#5FB3B3"
color08 = "#D8DEE9"
color09 = "#343D46"
color10 = "#EC5F67"
color11 = "#99C794"
color12 = "#FAC863"
color13 = "#6699CC"
color14 = "#C594C5"
color15 = "#5FB3B3"
color16 = "#D8DEE9"

colorTrayer :: String
colorTrayer = "--tint 0x2E3440"
