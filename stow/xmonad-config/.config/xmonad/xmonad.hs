  -- Base
import XMonad

  -- Utilities
import XMonad.Util.EZConfig
import XMonad.Util.Loggers

  -- Hooks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.FadeWindows

  -- Layouts
import XMonad.Layout.ThreeColumns

  -- Layout Modifiers
import XMonad.Layout.Renamed
import XMonad.Layout.Magnifier
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders

  -- ColorScheme
import Colors.Nord

  -- Variables
myFont :: String
myFont = ""

myModMask :: KeyMask
myModMask = mod4Mask

myBrowser :: String
myBrowser = ""

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs'"

myEditor :: String
myEditor = "emacsclient -c -a 'emacs'"

myBorderWidth :: Dimension
myBorderWidth = 2

myNormColor :: String
myNormColor = colorBack

myFocusColor :: String
myFocusColor = color15

  -- Layout Hook
myLayoutHook = smartBorders $ tall ||| threeCol ||| Full
    where
	nmaster  = 1
	delta    = 3/100
	ratio    = 1/2
	tall     = renamed [Replace "Tall"]
	         $ smartSpacingWithEdge 4
	         $ Tall nmaster delta ratio
        threeCol = renamed [Replace "ThreeCol"]
	         $ smartSpacingWithEdge 4
	         $ magnifiercz' 1.3
		 $ ThreeColMid nmaster delta ratio

  -- Manage Hook
myManageHook :: ManageHook
myManageHook = composeAll
    [ isDialog            --> doFloat
    , isFullscreen        --> doFullFloat
    , className =? "Gimp" --> doFloat
    ]

  -- XMobar Pretty Printer
myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = xmobarColor color08 "" . xmobarFont 1 . pad $ "|"
    , ppTitleSanitize   = xmobarStrip
    , ppLayout          = xmobarColor color06 "" . pad
    , ppCurrent         = xmobarColor color04 "" . xmobarBorder "Bottom" color04 3 . pad
    , ppHidden          = xmobarColor color05 "" . xmobarBorder "Top" color05 3 . pad
    , ppHiddenNoWindows = xmobarColor color05 "" . pad
    , ppUrgent          = xmobarColor color02 "" . wrap "!" "!"
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    }
  where
    formatFocused   = xmobarColor color04 "" . xmobarBorder "Bottom" color04 3 . wrap "[" "]" . pad . ppWindow
    formatUnfocused = xmobarColor color05 "" . wrap "[" "]" . pad . ppWindow

    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 20

  -- Fade Hook - Fade Inactive Windows
myFadeHook = composeAll
    [ opaque
    , isUnfocused --> transparency 0.25
    ]

  -- Custom Key Bindings
myKeys =
    [ ("M-p", spawn "rofi -show run") ]

  -- Main Function
main :: IO ()
main = xmonad
     . ewmhFullscreen
     . ewmh
     . withEasySB (statusBarProp "xmobar" (pure myXmobarPP)) defToggleStrutsKey
     $ def
         { modMask            = myModMask                     -- Rebind Mod to the Super Key
         , layoutHook         = myLayoutHook                  -- Use Custom Layouts
         , terminal           = "x-terminal-emulator"         -- Deafult Terminal Provided by Update-Alternatives
         , borderWidth        = myBorderWidth                 -- Custom Window Border Width
         , normalBorderColor  = myNormColor                   -- Custom Unfocused Window Border Color
         , focusedBorderColor = myFocusColor                  -- Custom Focused Window Border Color
         , manageHook         = myManageHook                  -- Match on Certain Windows
         , logHook            = fadeWindowsLogHook myFadeHook -- Fade Inactive Windows
         , handleEventHook    = fadeWindowsEventHook          -- Fade Inactive Windows
         } `additionalKeysP` myKeys                           -- Use Custom Keybindings
