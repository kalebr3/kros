# ADDING TO THE PATH

set -e fish_user_paths                                                      # Remove Current PATH
set -U fish_user_paths $HOME/.cargo/bin $HOME/.local/bin $HOME/Applications $HOME/.config/emacs/bin $fish_user_paths # Set New PATH

# EXPORTS

set fish_greeting                       # Suppress FISH's Intro Message
set TERM "xterm-256color"               # Set Terminal Type
set EDITOR "emacsclient -t -a ''"       # Set EDITOR to use Emacs in Terminal Mode
set VISUAL "emacsclient -c -a emacs"    # Set VISUAL to use Emacs in GUI Mode

# BAT AS MANPAGER

set -x MANPAGER "sh -c 'col -bx | batcat -l man -p'"

# AUTOCOMPLETE AND HIGHLIGHT COLORS

set fish_color_normal brcyan
set fish_color_autosuggestion '#7D7D7D'
set fish_color_command brcyan
set fish_color_error '#FF6C6B'
set fish_color_param brcyan

# ALIASES

# Vim and Emacs
alias vim="nvim"
alias doomsync="~/.config/emacs/bin/doom sync"
alias doomdoctor="~/.config/emacs/bin/doom doctor"
alias doomupgrade="~/.config/emacs/bin/doom upgrade"
alias doompurge="~/.config/emacs/bin/doom purge"
# ls to exa
alias ls="exa -l --color=always --group-directories-first"
alias la="exa -al --color=always --group-directories-first"
alias ll="exa -l --color=always --group-directories-first"
alias lt="exa -alT --color=always --group-directories-first"

# INITIALIZE STARSHIP PROMPT

starship init fish | source
