#!/bin/bash

echo "WELCOME TO THE KROS INSTALLER"

# Short circuit if run as root user
if [ $USER = root ]; then
	echo "DO NOT RUN THIS SCRIPT AS THE ROOT USER"
	echo "This script stows configurations to the home directory"
	echo "of the user running the script. For actions requiring"
	echo "elevation you will be prompted to input your password."
	exit 1
fi

# Set Script Variables
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
SCRIPT_USER=$(whoami)

# Update Packages and Upgrade OS
sudo apt-get update && sudo apt-get upgrade -y

# Install Required Packages
xargs -r sudo apt-get -y install < ./packages

# Create $HOME/.config directory
if [ ! -d $HOME/.config ]; then
	mkdir $HOME/.config
fi

# Create $HOME/.config/xmonad directory
if [ ! -d $HOME/.config/xmonad ]; then
	mkdir $HOME/.config/xmonad
fi

# Install Xmonad
cd $HOME/.config/xmonad
git clone https://github.com/xmonad/xmonad
git clone https://github.com/xmonad/xmonad-contrib
sudo stack upgrade
stack init
stack install

# Stow Configurations
cd $SCRIPT_DIR
stow --dir=./stow --target=$HOME --verbose alacritty-config
stow --dir=./stow --target=$HOME --verbose doom-config
stow --dir=./stow --target=$HOME --verbose fish-config
stow --dir=./stow --target=$HOME --verbose nvim-config
stow --dir=./stow --target=$HOME --verbose rofi-config
stow --dir=./stow --target=$HOME --verbose volumeicon-config
stow --dir=./stow --target=$HOME --verbose xmobar-config
stow --dir=./stow --target=$HOME --verbose xmonad-config
stow --dir=./stow --target=$HOME --verbose xorg-config
sudo stow --dir=./stow --target=/ --verbose font-config
sudo stow --dir=./stow --target=/ --verbose network-config
sudo stow --dir=./stow --target=/ --verbose sddm-config

cp -r ./stow/nitrogen-config/.config/nitrogen $HOME/.config/nitrogen

# Install Vim-Plug
curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Install Doom Emacs
git clone --depth 1 https://github.com/hlissner/doom-emacs $HOME/.config/emacs
$HOME/.config/emacs/bin/doom -y install

# Install Brave Internet Browser
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt-get update
sudo apt-get install -y brave-browser

# Install Alacritty
sudo apt-get install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
curl -sSf https://sh.rustup.rs | sh -s -- -y
source $HOME/.cargo/env
cd /tmp
git clone https://github.com/alacritty/alacritty.git
cd /tmp/alacritty
rustup override set stable
rustup update stable
cargo build --release
sudo tic -xe alacritty,alacritty-direct extra/alacritty.info
sudo cp target/release/alacritty /usr/local/bin
sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
sudo mkdir /usr/local/share/man/man1
gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz
gzip -c extra/alacritty-msg.man | sudo tee /usr/local/share/man/man1/alacritty-msg.1.gz
mkdir -p $HOME/.config/fish/completions
cp extra/completions/alacritty.fish $HOME/.config/fish/completions/alacritty.fish
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/alacritty 50

# Install Starship Prompt
curl -sS https://starship.rs/install.sh | sh -s -- -y

# Clone Wallpaper Repository
sudo git clone https://gitlab.com/kalebr3/wallpapers.git /usr/share/wallpapers-kros

# Set Login Shell to Fish
sudo chsh -s /usr/bin/fish $SCRIPT_USER

# Setup Complete Reboot
sudo systemctl reboot
